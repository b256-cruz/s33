"use strict";

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => {
    const titles = json.map(todo => todo.title);
    console.log(titles);
})

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(`The item ${json.title} on the list has a status of ${json.completed}`))


fetch("https://jsonplaceholder.typicode.com/posts", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    completed: "false",
    title: "Created To do list",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: "update to do list item",
    description: "To update to my list with a different data structure",
    status: "pending",
    dateCompleted: "pending",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
method: 'PATCH',
headers: {
    'Content-Type': 'application/json'
  },
body: JSON.stringify({
    dateCompleted: "pending",
  })
})
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
})

